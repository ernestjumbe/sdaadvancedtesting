import json

class EmployeeDB:
    def __init__(self):
        self.__data = None
    
    def connect(self, data_file):
        with open(data_file) as json_file:
            self.__data = json.load(json_file)
    
    def get_employee(self, name):
        target_employee = None
        for employee in self.__data['employees']:
            if employee['name'] == name:
                target_employee = employee
        
        if target_employee:
            return target_employee
        else:
            raise ItemNotFound
    
    def create_employee(self, employee_object):
        if 'name' not in employee_object:
            raise Fieldrequired('You must provide a name for a new employee')
        if 'salary' not in employee_object:
            raise Fieldrequired('You must provide a salary a new employee')

        self.__data['employees'].append(employee_object)
        # {name': 'Sally', 'salary': 48000}
        return self.get_employee(employee_object['name'])
    
    def employee_raise(self, name, percentage_raise):
        return NotImplemented
    
    def close(self):
        """Closes DB 'connection'"""
        pass


class ItemNotFound(Exception):
    pass

class Fieldrequired(Exception):
    pass